---
title: "Dessins"
type: "gallery"
url: "/drawings"
---

Il est possible de créer plusieurs galeries (simplement plusieurs répertoires) : voir sur https://gitlab.com/elelay/testnlify/ (accès privé)
comment c'est fait.

Il manque encore la possibilité d'afficher un texte sous les images, en intégrant [photoswipe](https://photoswipe.com/).
