---
title: "Photos"
type: "gallery"
url: "/photos"
---

Cette galerie est générée à partir des images contenues dans le sous-répertoire `images`.

**Important** penser à regénérer les imagettes avec cette commande :

```bash
# rm small/*; for f in *.*; do convert -resize 460x -auto-orient -strip $f small/$f; done
```